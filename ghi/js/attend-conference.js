window.addEventListener('DOMContentLoaded', async () => {
  const selectTag = document.getElementById('conference');

  const url = 'http://localhost:8000/api/conferences/';
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();

    for (let conference of data.conferences) {
      const option = document.createElement('option');
      option.value = conference.href;
      option.innerHTML = conference.name;
      selectTag.appendChild(option);
    }

    // Here, add the 'd-none' class to the loading icon
    const loadingIcon = document.getElementById('loading-conference-spinner')
    loadingIcon.classList.add('d-none')
    // Here, remove the 'd-none' class from the select tag
    selectTag.classList.remove('d-none')
    }

    // Get the attendee form element by its id
    const attendeeForm = document.getElementById('create-attendee-form')
        // Add an event handler for the submit event
    attendeeForm.addEventListener("submit", async (event) => {
        // Prevent the default from happening
        event.preventDefault();
        // Create a FormData object from the form
        const formData = new FormData(attendeeForm);
        // Get a new object from the form data's entries
        const json = JSON.stringify(Object.fromEntries(formData))
        // Create options for the fetch
        const value = document.getElementById('conference').value;
        const attendeesUrl = `http://localhost:8001${value}attendees/`;
        const fetchConfig = {
            method: 'post',
            body : json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        // Make the fetch using the await keyword to the URL
        const response = await fetch(attendeesUrl, fetchConfig);
        if (response.ok) {
            const alertSuccess = document.getElementById("success-message")
            alertSuccess.classList.remove('d-none')
            attendeeForm.classList.add('d-none')
        }
    });
});
